#ifndef CAMERA_FRAME_WIDGET_H
#define CAMERA_FRAME_WIDGET_H

#include <QObject>
#include <QWidget>
#include <QImage>

/// \brief The CameraFrameWidget class виджет для отображения фреймов (кадров)
class CameraFrameWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CameraFrameWidget(QWidget *parent = nullptr);
public slots:
    /// \brief onReceiveFrame слот приёма фрейма (кадра)
    /// \param frame
    void onReceiveFrame(QImage frame);
private:
    /// \brief m_currentFrame текущий фрейм (кадр)
    /// последний принятый
    QImage m_currentFrame;
    // QWidget interface
protected:
    void paintEvent(QPaintEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;
};

#endif // CAMERA_FRAME_WIDGET_H
