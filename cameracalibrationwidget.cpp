#include "cameracalibrationwidget.h"
#include "ui_cameracalibrationwidget.h"

CameraCalibrationWidget::CameraCalibrationWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CameraCalibrationWidget)
{
    ui->setupUi(this);
    this->init_variables();
    this->init_image_processor_thread();
}

CameraCalibrationWidget::~CameraCalibrationWidget()
{
    image_processor->kill();
    QThread::msleep(300);
    image_processing_thread.quit();
    image_processing_thread.wait();
    delete ui;
}

void CameraCalibrationWidget::add_new_frame_to_calibration_list(QImage src_img)
{
//    QListWidgetItem item("something", ui->listWidget);
//    item.setData(Qt::DecorationRole, QPixmap::fromImage(src_img));
//    ui->listWidget->

    //QListWidget *lw = new QListWidget(this);
    QListWidgetItem *itm = new QListWidgetItem(tr("one"));
    itm->setIcon(QIcon(QPixmap::fromImage(src_img)));
    ui->listWidget->addItem(itm);
}

void CameraCalibrationWidget::init_image_processor_thread()
{
    image_processor = new ImageProcessor;
    image_processor->moveToThread(&image_processing_thread);
    //удаление
    connect(&image_processing_thread, &QThread::finished, image_processor, &QObject::deleteLater);
    //старт, соединить с кнопкой на форме
    connect(this, &CameraCalibrationWidget::start_show, image_processor, &ImageProcessor::doWork);
    //изображение, соединить с кастомным виджетом
    connect(image_processor, &ImageProcessor::resultReady, ui->widget_camera_frame,&CameraFrameWidget::onReceiveFrame);
    //отправить изображение в список
    connect(image_processor, &ImageProcessor::resultReady, this,&CameraCalibrationWidget::add_new_frame_to_calibration_list);

    image_processing_thread.start();
    emit start_show();
}

void CameraCalibrationWidget::init_variables()
{
    current_cameraframewidget_state = 0;
    ui->listWidget->setIconSize(QSize(200,200));
    ui->listWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
}

void CameraCalibrationWidget::on_pushButton_clicked()
{
    if(current_cameraframewidget_state==0)
    {
        current_cameraframewidget_state = 1;
        image_processor->start();
        ui->pushButton->setText("Приостановить");
        ui->pushButton_2->setEnabled(false);
        return;
    }
    if(current_cameraframewidget_state==1)
    {
        // qDebug()<<__FUNCTION__<<__LINE__;
        current_cameraframewidget_state = 0;
        image_processor->stop();
        ui->pushButton->setText("Начать");
        ui->pushButton_2->setEnabled(true);

        return;
    }
}
void CameraCalibrationWidget::on_pushButton_2_clicked()
{

}
void CameraCalibrationWidget::on_pushButton_3_clicked()
{
ui->listWidget->clear();
}
