#include "cameraframewidget.h"

#include <QPainter>

CameraFrameWidget::CameraFrameWidget(QWidget *parent) : QWidget(parent) {
}

void CameraFrameWidget::onReceiveFrame(QImage frame) {
    m_currentFrame = frame;
    repaint();
}


void CameraFrameWidget::paintEvent(QPaintEvent *event) {
    Q_UNUSED(event)
    auto initWidget = [this](QPainter& painter) -> void {
        // настраиваем рендер на хорошее качество
        painter.setRenderHint(QPainter::HighQualityAntialiasing, true);
        painter.setRenderHint(QPainter::TextAntialiasing, true);
        painter.setRenderHint(QPainter::SmoothPixmapTransform, true);
    };
    if (m_currentFrame.isNull())
        return;
    QPainter painter(this);
    initWidget(painter);
    auto currentSize = this->size();    // текущий размер виджета
    if (
            (currentSize.width() != m_currentFrame.width()) ||
            (currentSize.height() != m_currentFrame.height())
       ) {
        // адаптировать размер фрейма к текущему размеру
        QImage tempScaledFrame = m_currentFrame.scaled(currentSize);//, Qt::KeepAspectRatio);
        painter.drawImage(QPoint(0, 0), tempScaledFrame);
    } else {
        painter.drawImage(QPoint(0, 0), m_currentFrame);
    }
}


void CameraFrameWidget::resizeEvent(QResizeEvent *event) {
    Q_UNUSED(event)
    repaint();
}
