#-------------------------------------------------
#
# Project created by QtCreator 2019-11-07T17:44:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dummy_app
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    cameracalibrationwidget.cpp \
    cameraframewidget.cpp \
    imageprocessor.cpp

HEADERS  += mainwindow.h \
    cameracalibrationwidget.h \
    cameraframewidget.h \
    imageprocessor.h

FORMS    += mainwindow.ui \
    cameracalibrationwidget.ui

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../libs/opencv/opencv/build/x64/vc14/lib/ -lopencv_world330
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../libs/opencv/opencv/build/x64/vc14/lib/ -lopencv_world330d

##INCLUDEPATH += d:/libs/opencv/opencv/build/include
#INCLUDEPATH += $$PWD/../../../../../libs/opencv/opencv/build/include
#DEPENDPATH += $$PWD/../../../../../libs/opencv/opencv/build/include

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../libraries/opencv/3.4.0_vc14_vc15/build/x64/vc14/lib/ -lopencv_world340
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../libraries/opencv/3.4.0_vc14_vc15/build/x64/vc14/lib/ -lopencv_world340d

INCLUDEPATH += $$PWD/../../../../libraries/opencv/3.4.0_vc14_vc15/build/include
DEPENDPATH += $$PWD/../../../../libraries/opencv/3.4.0_vc14_vc15/build/include
