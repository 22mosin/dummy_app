#ifndef IMAGEPROCESSOR_H
#define IMAGEPROCESSOR_H

#include <QObject>
#include <QImage>
#include <QThread>
#include <QDebug>

//OpenCV:
#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
using namespace cv;

//std:
#include <atomic>
#include <vector>

class ImageProcessor : public QObject
{
    Q_OBJECT
private: //variables
    //работает ли сейчас основной цикл
    std::atomic_bool is_work;
    //закончить функцию doWork()
    std::atomic_bool is_alive;
public:
    explicit ImageProcessor(QObject *parent = 0);
    void start(){is_work = true;}
    void stop(){is_work = false;}
    void kill(){is_alive = false;}
public slots:
    void doWork();
signals:
    void resultReady(QImage dst_img);
};

#endif // IMAGEPROCESSOR_H
