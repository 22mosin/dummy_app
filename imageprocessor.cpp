#include "imageprocessor.h"

QImage Mat2QImage(cv::Mat const& src)
{
    cv::Mat temp; // make the same cv::Mat
    cvtColor(src, temp,CV_BGR2RGB); // cvtColor Makes a copt, that what i need
    QImage dest((const uchar *) temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
    dest.bits(); // enforce deep copy, see documentation
    // of QImage::QImage ( const uchar * data, int width, int height, Format format )
    return dest;
}

cv::Mat QImage2Mat(QImage const& src)
{
    cv::Mat tmp(src.height(),src.width(),CV_8UC3,(uchar*)src.bits(),src.bytesPerLine());
    cv::Mat result; // deep copy just in case (my lack of knowledge with open cv)
    cvtColor(tmp, result,CV_BGR2RGB);
    return result;
}

ImageProcessor::ImageProcessor(QObject *parent) : QObject(parent)
{
    is_work = false;
}

void ImageProcessor::doWork()
{
    QImage dst_img;
    VideoCapture capture;
    capture.open(0);
    Mat view;

    for (;;)
    {

        if (!is_alive)
        {
            break;
        }
        if(!is_work){
            //TODO: потом сделать поменьше?
            QThread::sleep(1);
        }
        else if (capture.isOpened())
        {
            Mat view0,viewGray;
            capture >> view0;
            view0.copyTo(view);
            Size patternsize(8,6); //interior number of corners
            cvtColor(view, viewGray, COLOR_BGR2GRAY);
            std::vector<Point2f> corners;
            bool patternfound = findChessboardCorners(viewGray, patternsize, corners,
                                                      CALIB_CB_ADAPTIVE_THRESH + CALIB_CB_NORMALIZE_IMAGE
                                                      + CALIB_CB_FAST_CHECK);

            if(patternfound)
            {
                cornerSubPix(viewGray, corners, Size(11, 11), Size(-1, -1),
                             TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));

                drawChessboardCorners(view, patternsize, Mat(corners), patternfound);
            }
            QImage image_1 = Mat2QImage(view);
            emit resultReady(image_1);
            // QThread::msleep(100);
        }
        else
        {
            qDebug()<<"Проблемки с вебкой или типа того..";
        }
    }
}

