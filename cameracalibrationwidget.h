#ifndef CAMERACALIBRATIONWIDGET_H
#define CAMERACALIBRATIONWIDGET_H

//Qt:
#include <QWidget>
#include <QDebug>
#include <qthread>

//this:
#include "imageprocessor.h"
 #include <QListWidget>
#include <QVector>

namespace Ui {
class CameraCalibrationWidget;
}

class CameraCalibrationWidget : public QWidget
{
    Q_OBJECT

private:
    QThread image_processing_thread;
    ImageProcessor *image_processor;

    //состояние окна отображения
    //0 - отображение выключено (добавить фоновый рисунок)
    //1 - отображение без исправления дисторсии
    //2 - исправление дисторсии
    int current_cameraframewidget_state;

public:
    explicit CameraCalibrationWidget(QWidget *parent = 0);
    ~CameraCalibrationWidget();

public slots:
    void add_new_frame_to_calibration_list(QImage src_img);

private slots:
    void on_pushButton_3_clicked();
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::CameraCalibrationWidget *ui;
    QVector<QListWidgetItem> m_calibrations_frames_list;

signals:
    void start_show();
    void stop_show();

private:
    void init_image_processor_thread();
    void init_variables();
};

#endif // CAMERACALIBRATIONWIDGET_H
